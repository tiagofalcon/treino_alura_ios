//
//  ContatoDao.h
//  AgendaDeContatos
//
//  Created by Tiago Falcon Lopes on 12/01/21.
//

#import <Foundation/Foundation.h>
#import "Contato.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContatoDao : NSObject

@property NSMutableArray *contatos;

- (void) adicionaContato: (Contato *) contato;
- (NSInteger) total;
- (Contato *) contatoDoIndice:(NSInteger) indice;
- (NSInteger) indiceDoContato:(Contato *) contato;
+ (ContatoDao *) contatoDaoInstance;
- (void) removeContato: (Contato *) contato;

@end

NS_ASSUME_NONNULL_END

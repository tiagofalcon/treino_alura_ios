//
//  AppDelegate.h
//  AgendaDeContatos
//
//  Created by Tiago Falcon Lopes on 12/01/21.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
@property (strong, nonatomic) UIWindow *window;

@end


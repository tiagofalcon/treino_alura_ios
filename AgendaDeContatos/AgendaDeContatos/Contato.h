//
//  Contato.h
//  AgendaDeContatos
//
//  Created by Tiago Falcon Lopes on 12/01/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Contato : NSObject

@property NSString *nome;
@property NSString *endereco;
@property NSString *email;
@property NSString *telefone;
@property NSString *site;

@end

NS_ASSUME_NONNULL_END

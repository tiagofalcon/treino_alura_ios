//
//  ListaDeContatosViewController.h
//  AgendaDeContatos
//
//  Created by Tiago Falcon Lopes on 12/01/21.
//

#import <UIKit/UIKit.h>
#import "ContatoDao.h"
#import "ViewController.h"

@interface ListaDeContatosViewController : UITableViewController<ViewControllerDelegate>

@property ContatoDao *dao;
@property Contato *contatoSelecionado;
@property NSInteger linhaSelecionada;

@end

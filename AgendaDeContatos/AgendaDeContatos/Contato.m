//
//  Contato.m
//  AgendaDeContatos
//
//  Created by Tiago Falcon Lopes on 12/01/21.
//

#import "Contato.h"

@implementation Contato

NSString *nome;

- (NSString *)description {
    NSString *dados = [NSString stringWithFormat:@"Nome: %@ Endereço: %@ E-mail: %@ Stie: %@ Telefone: %@", self.nome, self.endereco, self.email, self.site, self.telefone];
    return dados;
}

@end

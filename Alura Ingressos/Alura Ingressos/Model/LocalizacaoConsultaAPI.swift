//
//  LocalizacaoConsultaAPI.swift
//  Alura Ingressos
//
//  Created by Tiago Falcon Lopes on 13/01/21.
//

import UIKit
import Alamofire

class LocalizacaoConsultaAPI: NSObject {

    func consultaViaCepAPI(_ cep:String, sucesso:@escaping(_ localizacao:Localizacao) -> Void, falha:@escaping(_ error:AFError) -> Void) {
        AF.request("https://viacep.com.br/ws/\(cep)/json/").validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let resultado = response.value as? Dictionary<String, String> {
                    let localizacao = Localizacao(resultado)
                    sucesso(localizacao)
                }
                break
            case .failure:
                falha(response.error!)
                break
            }
        }
    }
    
}

//
//  ViewController.swift
//  Animacoes
//
//  Created by Tiago Falcon Lopes on 22/01/21.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var viewAnimada: UIView!
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - IBAction
    
    
    @IBAction func botaoIniciarAnimacao(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 1.5, options: [.curveLinear], animations: {
            self.viewAnimada.frame = CGRect(x: self.viewAnimada.frame.origin.x, y: 420.0, width: self.viewAnimada.frame.size.width, height: self.viewAnimada.frame.size.height)
        }, completion: nil)
    }
}


//
//  GeradorDePagamento.swift
//  Leilao
//
//  Created by Tiago Falcon Lopes on 15/01/21.
//  Copyright © 2021 Alura. All rights reserved.
//

import Foundation

class GeradorDePagamento {
    
    private var leiloes:LeilaoDao
    private var avaliador:Avaliador
    private var repositorioDePagamento:RepositorioDePaganento
    private var data:Date
    
    init(_ leiloes:LeilaoDao, _ avaliador:Avaliador, _ repositorioDePagamento:RepositorioDePaganento, _ data:Date) {
        self.leiloes = leiloes
        self.avaliador = avaliador
        self.repositorioDePagamento = repositorioDePagamento
        self.data = data
    }
    
    convenience init(_ leiloes:LeilaoDao, _ avaliador:Avaliador, _ repositorioDePagamento:RepositorioDePaganento) {
        self.init(leiloes, avaliador, repositorioDePagamento, Date())
    }
    
    func gera() {
        let leiloesEncerrados = self.leiloes.encerrados()
        for leilao in leiloesEncerrados {
            try? avaliador.avalia(leilao: leilao)
            
            let novoPagamento = Pagamento(avaliador.maiorLance(), proximoDiaUtil())
            repositorioDePagamento.salva(novoPagamento)
        }
    }
    
    func proximoDiaUtil() -> Date {
        var dataAtual = data
        while Calendar.current.isDateInWeekend(dataAtual) {
            dataAtual = Calendar.current.date(byAdding: .day, value: 1, to: dataAtual)!
        }
        return dataAtual
    }
    
}

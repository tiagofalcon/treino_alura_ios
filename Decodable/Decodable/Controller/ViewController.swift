//
//  ViewController.swift
//  Decodable
//
//  Created by Tiago Falcon Lopes on 14/01/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        getViagens()
    }


    func getViagens() {
        guard let json = ViagemAPI().getViagens() else { return }
        guard let jsonData = Viagem.converteListaParaData(json) else { return }
        guard let listaDeViagem = Viagem.decodificaViagem(jsonData) else { return }
        for viagem in listaDeViagem {
            print(viagem.titulo)
        }
    }
    
}


//
//  ViagemAPI.swift
//  Decodable
//
//  Created by Tiago Falcon Lopes on 14/01/21.
//

import Foundation

class ViagemAPI {
    
    func getViagens() -> [[String: Any]]? {
        if let caminho = Bundle.main.url(forResource: "viagens", withExtension: "json") {
            let json:Data = try! Data(contentsOf: caminho)
            do {
                let viagens = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                let listaDeViagens = viagens as? [[String:Any]]
                return listaDeViagens
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}

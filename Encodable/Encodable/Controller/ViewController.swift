//
//  ViewController.swift
//  Encodable
//
//  Created by Tiago Falcon Lopes on 14/01/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        favoritaViagem()
    }
    
    func favoritaViagem() {
        let viagem = Viagem(1, "Cancun - Mexico", 10, "6.500,00", "Quintana Roo")
        
        let viagemCodificada = try? JSONEncoder().encode(viagem)
        guard let viagemData = viagemCodificada else { return }
        
        if let json = String(data: viagemData, encoding: .utf8) {
            print(json)
        }
    }


}

